package br.com.ilhasoft.support.view;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;

/**
 * Created by danielsan on 8/25/15.
 */
public class Activities {

    public static void hideSoftKeyboard (@NonNull Context context, @NonNull View view)     {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

}
