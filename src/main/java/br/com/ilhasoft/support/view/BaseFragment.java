package br.com.ilhasoft.support.view;

import android.content.Context;
import android.content.DialogInterface;

import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;

/**
 * Created by danielsan on 9/15/15.
 */
public abstract class BaseFragment extends Fragment {

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof BaseActivity))
            throw new ClassCastException(context + " must extend BaseActivity.");
    }

    public final BaseActivity getBaseActivity() {
        return (BaseActivity) this.getActivity();
    }

    public void showToast(@StringRes int textRes) {
        this.showToast(this.getString(textRes));
    }

    public void showToast(String text) {
        this.getBaseActivity().showToast(text);
    }

    public void showQuestion(@StringRes int titleRes, @StringRes int messageRes,
                             DialogInterface.OnClickListener onClickListener) {
        this.getBaseActivity().showQuestion(titleRes, messageRes, onClickListener);
    }

    public void showQuestion(String title, String message,
                             DialogInterface.OnClickListener onClickListener) {
        this.getBaseActivity().showQuestion(title, message, onClickListener);
    }

    public void showLoading(@StringRes int messageRes) {
        this.getBaseActivity().showLoading(messageRes);
    }

    public void showLoading(String message) {
        this.getBaseActivity().showLoading(message);
    }

    public void showLoading() {
        this.getBaseActivity().showLoading();
    }

    public void dismissLoading() {
        this.getBaseActivity().dismissLoading();
    }

}
