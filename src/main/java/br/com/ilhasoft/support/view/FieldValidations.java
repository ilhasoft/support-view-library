package br.com.ilhasoft.support.view;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by danielsan on 8/18/15.
 */
public class FieldValidations {

    public static boolean checkIfIsChecked(RadioGroup radioGroup, String fieldToSelect) {
        if (radioGroup == null)
            return false;

        boolean result = (radioGroup.getCheckedRadioButtonId() != View.NO_ID);
        if (!result)
            Toast.makeText(radioGroup.getContext(),
                           radioGroup.getContext().getString(R.string.error_message_select, fieldToSelect),
                           Toast.LENGTH_SHORT)
                 .show();
        return result;
    }

    public static boolean checkIfDateIsValid(EditText editText, String dateLeft, String dateRight,
                                             String datePattern) {
        try {
            SimpleDateFormat simpleDateFormat = FieldValidations.getDateFormat(datePattern);
            return FieldValidations.checkIfDateIsValid(editText, simpleDateFormat.parse(dateLeft),
                                                       simpleDateFormat.parse(dateRight), simpleDateFormat);
        } catch (Exception ignored) {
            return false;
        }
    }

    public static boolean checkIfDateIsValid(EditText editText, Date dateLeft, Date dateRight,
                                             String datePattern) {
        return FieldValidations.checkIfDateIsValid(editText, dateLeft, dateRight,
                                                   FieldValidations.getDateFormat(datePattern));
    }

    public static boolean checkIfDateIsValid(EditText editText, Date dateLeft, Date dateRight,
                                             SimpleDateFormat dateFormat) {
        if (!FieldValidations.checkIfIsNotEmpty(editText))
            return false;

        if (!FieldValidations.checkIfLengthIsCorrect(editText, 10))
            return false;

        Date date;
        boolean result;
        try {
            date = dateFormat.parse(editText.getText().toString());
            result = (date.after(dateLeft) && date.before(dateRight));
        } catch (Exception ignored) {
            result = false;
        }

        if (!result)
            FieldValidations.setHintError(editText, R.string.error_message_invalide_date);

        return result;
    }

    private static SimpleDateFormat getDateFormat(String datePattern) {
        if (TextUtils.isEmpty(datePattern))
            return new SimpleDateFormat();
        else
            return new SimpleDateFormat(datePattern);
    }

    public static boolean checkIfLengthIsCorrect(EditText editText, int lengthTarget) {
        if (!FieldValidations.checkIfIsNotEmpty(editText))
            return false;

        if (editText.getText().toString().trim().length() < lengthTarget) {
            FieldValidations.setHintError(editText, R.string.error_message_incomplete);
            return false;
        }

        return true;
    }

    public static boolean checkIfIsNotEmpty(EditText editText) {
        if (editText == null)
            return false;

        String text = editText.getText().toString();
        if (text.isEmpty()) {
            FieldValidations.setHintError(editText, R.string.error_message_to_be_provided);
            return false;
        }

        if (text.trim().isEmpty()) {
            FieldValidations.setHintError(editText, R.string.error_message_can_not_contain_only_spaces);
            return false;
        }

        return true;
    }

    public static boolean validatePassword(EditText editText) {
        if (editText == null)
            return false;

        String password = editText.getText().toString();
        if (password.contains(" ")) {
            FieldValidations.setHintError(editText, R.string.error_message_can_not_contain_spaces);
            return false;
        }

        password = password.trim();
        if (password.isEmpty()) {
            FieldValidations.setHintError(editText, R.string.error_message_to_be_provided);
            return false;
        }

        int passwordLength = password.length();
        if (passwordLength < 6 || passwordLength > 20) {
            FieldValidations.setHintError(editText, R.string.error_message_password_length);
            return false;
        }

        return true;
    }

    public static boolean checkPasswords(EditText password, EditText passwordConfirm) {
        boolean result = FieldValidations.validatePassword(password);
        result = FieldValidations.validatePassword(passwordConfirm) && result;
        boolean tempResult = password.getText().toString().trim().equals(passwordConfirm.getText().toString().trim());
        result = tempResult && result;

        if (!tempResult)
            FieldValidations.setHintError(passwordConfirm, R.string.error_message_passwords_are_different);

        return result;
    }

    public static boolean validateCPF(EditText cpf) {
        boolean result = FieldValidations.checkIfLengthIsCorrect(cpf, 14);
        if (!result)
            return false;

        String rawCPF = cpf.getText().toString().trim().replaceAll("[^\\d]", "");

        int stepOne = (FieldValidations.cpfSum(rawCPF, 1) * 10) % 11;
        int stepTwo = (FieldValidations.cpfSum(rawCPF, 2) * 10) % 11;



        result = (stepOne == Character.getNumericValue(rawCPF.charAt(9))
                  && stepTwo == Character.getNumericValue(rawCPF.charAt(10)));
        if (!result)
            FieldValidations.setHintError(cpf, R.string.error_message_invalide_cpf);


        return result;
    }

    /**
     *
     * @param rawCPF raw CPF with length equal to 11.
     * @param step 1 or 2.
     * @return verification sum.
     */
    private static int cpfSum(String rawCPF, int step) {
        int sum = 0, count = 8 + step, baseMultiplier = 9 + step;
        for (int i = 0; i < count; ++i) {
            sum += ((baseMultiplier - i) * Character.getNumericValue(rawCPF.charAt(i)));
        }
        return sum;
    }

    public static void setHintError(@NonNull EditText editText, @StringRes int errorMessageRes) {
        TextInputLayout textInputLayout = null;
        if (editText.getParent() instanceof TextInputLayout)
            textInputLayout = (TextInputLayout) editText.getParent();
        else if (editText.getTag() instanceof TextInputLayout)
            textInputLayout = (TextInputLayout) editText.getTag();

        CharSequence fieldHint;
        if (textInputLayout == null) {
            fieldHint = FieldValidations.getFieldHint(editText, editText.getHint());
            editText.setError(editText.getContext().getString(errorMessageRes, fieldHint));
        } else {
            fieldHint = FieldValidations.getFieldHint(textInputLayout, textInputLayout.getHint());
            textInputLayout.setError(textInputLayout.getContext().getString(errorMessageRes, fieldHint));
        }
    }

    private static CharSequence getFieldHint(@NonNull View view, CharSequence fieldHint) {
        return TextUtils.isEmpty(fieldHint) ? view.getContext().getString(R.string.error_message_aux_this) : fieldHint;
    }

    public static void setTextWatcher(EditText editText) {
        if (editText != null && editText.getParent() instanceof TextInputLayout)
            FieldValidations.setTextWatcher(editText, (TextInputLayout) editText.getParent());
    }

    public static void setTextWatcher(EditText editText, final TextInputLayout textInputLayout) {
        if (editText != null && textInputLayout != null) {
            editText.setTag(textInputLayout);
            editText.addTextChangedListener(new SimpleTextWatcher(textInputLayout));
        }
    }

    private static class SimpleTextWatcher implements TextWatcher {
        private TextInputLayout textInputLayout;
        public SimpleTextWatcher(@NonNull final TextInputLayout textInputLayout) {
            this.textInputLayout = textInputLayout;
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            textInputLayout.setError(null);
            textInputLayout.setErrorEnabled(false);
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { }
        @Override
        public void afterTextChanged(Editable s) { }
    }

}
