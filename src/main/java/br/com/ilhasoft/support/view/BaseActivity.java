package br.com.ilhasoft.support.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

/**
 * Created by danielsan on 8/18/15.
 */
public abstract class BaseActivity extends AppCompatActivity {

    private ProgressDialog loadingDialog = null;

    protected void initObjects() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.initObjects();
        super.onCreate(savedInstanceState);
    }

    public void showLoading() {
        this.showLoading(-1);
    }

    public void showLoading(@StringRes int messageRes) {
        this.showLoading(this.getString((messageRes > 0) ? messageRes : R.string.message_wait_a_moment));
    }

    public void showLoading(String message) {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this);
            loadingDialog.setIndeterminate(true);
        }
        if (!TextUtils.isEmpty(message)) {
            loadingDialog.setMessage(message);
            loadingDialog.show();
        }
    }

    public void dismissLoading() {
        if (loadingDialog != null)
            loadingDialog.dismiss();
    }

    public void showToast(@StringRes int textRes) {
        Toast.makeText(this, textRes, Toast.LENGTH_SHORT).show();
    }

    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    public void showQuestion(@StringRes int titleRes, @StringRes int messageRes,
                             DialogInterface.OnClickListener onClickListener) {
        this.showQuestion((titleRes > 0) ? this.getString(titleRes) : "",
                          (messageRes > 0) ? this.getString(messageRes) : "",
                          onClickListener);
    }

    public void showQuestion(String title, String message,
                             DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (!TextUtils.isEmpty(title))
            builder.setTitle(title);
        if (!TextUtils.isEmpty(message))
            builder.setMessage(message);
        builder.setPositiveButton(R.string.yes, onClickListener);
        builder.setNegativeButton(R.string.no, onClickListener);
        builder.show();
    }

    public void startActivity(Class<? extends Activity> activityClass) {
        this.startActivity(new Intent(this, activityClass));
    }

    public void startActivityForResult(Class<? extends Activity> activityClass, int requestCode) {
        this.startActivityForResult(new Intent(this, activityClass), requestCode);
    }

    public void replaceActivity(Class<? extends Activity> activityClass) {
        this.startActivity(new Intent(this, activityClass));
        this.finish();
    }

    public void addFragment(@IdRes int containerViewId, Fragment fragment) {
        this.addFragment(containerViewId, fragment, true);
    }

    public void addFragment(@IdRes int containerViewId, Fragment fragment, boolean withBackStack) {
        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        if (withBackStack)
            fragmentTransaction.addToBackStack(fragment.toString());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.add(containerViewId, fragment).commit();
    }

    public void replaceFragment(@IdRes int containerViewId, Fragment fragment, boolean withBackStack) {
        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        if (withBackStack)
            fragmentTransaction.addToBackStack(fragment.toString());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.replace(containerViewId, fragment).commit();
    }

}
